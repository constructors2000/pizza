<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('stage');
            $table->string('account_name');
            $table->string('merchant_id');
            $table->string('processing_bank');
            $table->string('lead_source');
            $table->string('processing_stage');
            $table->date(  'close_date');
            $table->date(  'created_date');
            $table->string('initial_membership_payment');
            $table->string('udg');
            $table->string('package');
            $table->float( 'auth_fee');
            $table->string('promo_code');
            $table->string('id_status');
            $table->string('opportunity_owner');
            $table->string('marketing_source');
            $table->string('membership_buy_rate');
            $table->string('transaction_buy_rate');
            $table->integer('transactions');
            $table->string('commission');
            $table->string('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
