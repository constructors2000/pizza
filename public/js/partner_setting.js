$(document).ready(start);

function start() {
    var flag_submit = 0;
    /**
    * SETTING EVENTS
    * */
    $(document).bind("settingHandler", function (event, setting) {
        $('#modal-partner-settings').trigger('setting', setting);
    });

    /**
    * FILL ALL INPUT OF FORM
    * */
    $('#modal-partner-settings').bind('setting', function (event, setting) {

        if (setting && setting.id && setting.name && setting.email) {

            const name  = $(this).find("input[name='name']")[0];
            const email = $(this).find("input[name='email']")[0];
            const form  = $(this).find("form");

            if (form && name && email){
                const action = $(form).data("action")+'/'+setting.id;
                $(form).attr('action', action);
                $(name).val(setting.name);
                $(email).val(setting.email);
            }
        }
    });

    /**
    *
    * */
    $('.generate-new-password').on('click', function (e) {
        var input = $(this).closest('form').find('input[name="password"]');
        NewPasswordInput(input);
        e.preventDefault();
        e.stopPropagation();
    });

    /**
    * EMPTY ALL ERROR SETTINGS
    * */
    $('#modal-partner-settings').on('hide.bs.modal', function () {
        $(this).find(".invalid-feedback").empty();
        $(this).find('.is-invalid').removeClass('is-invalid');
    });

    /**
    * SUBMIT UPDATE FORM
    * */
    $('#modal-partner-settings form').on('submit', function (e) {
        e.preventDefault();

        if(flag_submit){
            return null;
        }

        flag_submit = 1;

        var form = $(this);
        var link = form.attr('action');

        $.ajax({
            method: "PATCH",
            url: link,
            data: form.serialize(),
        }).done(function(response) {
            if(response && response.data && response.data.id){
                $('#user_'+response.data.id).find('.name').html(response.data.name);
                $('#user_'+response.data.id).find('.email').html(response.data.email);
            }
            $('#modal-partner-settings').modal('hide');
            toastr.success('That partner has been update.','Success');
        }).fail(function (response) {
            console.error('fail',response);
            if(response && response.responseJSON && response.responseJSON.errors) {
                const errors = response.responseJSON.errors;
                for (var item in errors) {
                    var input = $(form).find("input[name='" + item + "']");
                    $(input).addClass('is-invalid', 2000);
                    $(input).siblings(".invalid-feedback").append('<strong>' + errors[item][0] + '</strong>');
                }
                setTimeout(function () {
                    $('.is-invalid').siblings(".invalid-feedback").empty();
                    $('.is-invalid').removeClass('is-invalid', 2000);
                }, 4000);
            }
            toastr.error('That partner has been setting fail update.','Error');
        }).always(function() {
            flag_submit = 0;
        });

    });

    /**
     *
     * */
    function NewPasswordInput(input = null) {
        var password = new RandExp(/\w{6,8}[0-9]{1,2}/).gen();
        if (input){
            $(input).val(password);
        }
        return password;
    }
}
