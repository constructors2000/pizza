$(document).ready(function () {
    /**
     * SETTING: Header for authorization ajax
     * */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /**
     * SETTING: position of toast
     * */
    toastr.options = {
        "positionClass": "toast-bottom-left"
    }

});
