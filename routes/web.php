<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', 'HomeController@index')->name('index');


Route::group(['prefix' => 'admin', 'as' => 'admin::'], function () {

    Route::get('/', 'AdminController@index')->name('index');

    Route::group(['prefix' => 'add', 'as' => 'add.'], function () {
        Route::post('/partner', 'AdminController@add_partner')->name('partner');
    });
});

/* Group routs for partners */
Route::group(['prefix' => 'partner', 'as' => 'partner::'], function () {
    Route::group(['prefix' => '{user}', 'as' => 'current.'], function () {

        Route::get('/', 'PartnerController@index')->name('index');
        Route::patch('/', 'PartnerController@update')->name('update');
        Route::delete('/', 'PartnerController@delete')->name('delete');
        Route::get('/email', 'PartnerController@sendEmailReminder')->name('email');

        Route::group(['prefix' => 'data', 'as' => 'data.'], function () {
            Route::post('/', 'AdminController@updateData')->name('update');
            Route::get('/csv', 'PartnerController@csv')->name('csv');
        });

    });
});


Auth::routes();

Route::group(['prefix' => 'preview', 'as' => 'preview.'], function (){
    Route::group(['prefix' => 'alert', 'as' => 'alert.'], function () {
        Route::get('/{message}/{time}/{redirect_to?}', 'PreviewController@index')->name('message')
            ->where(['time' => '[0-9]+', 'redirect_to' => '[a-z]+']);;
    });
});

Route::get('/home', 'HomeController@index')->name('home');
