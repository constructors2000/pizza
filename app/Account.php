<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
               "stage"
               ,"account_name"
               ,"merchant_id"
               ,"processing_bank"
               ,"lead_source"
               ,"processing_stage"
               ,"close_date"
               ,"created_date"
               ,"initial_membership_payment"
               ,"udg"
               ,"package"
               ,"auth_fee"
               ,"promo_code"
               ,"id_status"
               ,"opportunity_owner"
               ,"marketing_source"
               ,"membership_buy_rate"
               ,"transaction_buy_rate"
               ,"transactions"
               ,"commission"
               ,"status"
            ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'user_id', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
