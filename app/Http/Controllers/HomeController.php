<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        if($request->user()->hasRole('admin')) {
            return redirect()->route('admin::index');
        }
        elseif ($request->user()->hasRole('partner')){
            return redirect()->route('partner::current.index', [$request->user()]);
        } else{
            return redirect()->route('login');
        }
    }
}
