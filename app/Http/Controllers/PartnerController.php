<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Notifications\UpdateUser;
use App\Factorys\DataFactoryDownload;
use Illuminate\Support\Facades\Notification;

class PartnerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('current_user');
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, User $user)
    {
        $user = User::with(['last_note','accounts'])->withCount('accounts')->find($user->id);

        return view('partner.main')->with('user', $user);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, User $user)
    {
        // validator ony for paginate
        $this->validate($request, [
            'name'      => ['required','string','min:3','max:25'],
            'email' => [
                'required','email',
                Rule::unique('users')->ignore($user->id),
            ],
            'password'  => ['nullable', 'min:6', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X]).*$/']
        ]);

        $data = $request->only(['name', 'email', 'password']);

        // save password for email
        $_password = '';
        if(empty($data['password'])){
            array_forget($data, 'password');
        } else {
            $_password = $data['password'];
            $data['password'] = bcrypt($data['password']);
        }

        if ($user->update($data)){
            $user->refresh();

            if(!empty($data['password']) && !empty($_password)){
                $data['password'] =  $_password;
            }

            Notification::send($user, new UpdateUser($user, $data));

            return response()->json([
                'success' => true,
                'message' => 'Update partner successful',
                'data'=>$user], 200);
        } else{
            return response()->json([
                'success' => false,
                'message' => 'Update partner fails'], 400);
        }
    }

    /**
     * @param Request $request
     * @param User $user
     * @return string
     */
    public function csv(Request $request, User $user)
    {
        $factory = new DataFactoryDownload($user);

        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=galleries.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];
        return response()->download($factory->download('csv'), 'data.csv',$headers);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Request $request, User $user)
    {
        $user->roles()->detach();

        if ($user->forceDelete()){
            return response()->json([
                'success' => true,
                'message' => 'Delete successful'], 204);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Delete fails'], 400);
        }
    }

    /**
     * @param Request $request
     * @param User $user
     */
    public function sendEmailReminder(Request $request, User $user)
    {
        Mail::send('emails.reminder', ['user' => $user], function ($m) use ($user) {
            $m->from('gold.dev@gold-dev.com', 'Test not!');

            $m->to($user->email, $user->name)
                ->subject('Notification')
                ->line('You account was updated');
        });
    }

}
