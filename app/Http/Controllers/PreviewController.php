<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class PreviewController extends Controller
{
    public function index(Request $request, $message, $time=null, $redirect_to=null){

        $view =  view('preview.alert');
        if(!empty($time))
            $view->with('time', $time);

        if(!empty($redirect_to) && Route::has($redirect_to))
                $view->with('route', $redirect_to);

        if(!is_null($message))
            $view->with('message', $message);

        return $view;
    }
}
