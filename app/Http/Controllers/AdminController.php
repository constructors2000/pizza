<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Notifications\CreateUser;
use App\Notifications\UpdateData;
use App\Factorys\DataFactoryUpload;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_admin');
    }

    /**
     * Show the application dashboard for admin.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $users = User::with('last_note')->withCount('accounts')->get()->except(\Auth::id());

        return view('admin.main')->with('users',$users);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function add_partner(Request $request){

        // validator ony for paginate
        $this->validate($request, [
            'name'      => ['required','string','min:3','max:25'],
            'email'     => ['required','email','unique:users,email'],
            'password'  => ['required', 'min:6', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X]).*$/']
        ]);

        $data = $request->only(['name', 'email', 'password']);

        $_password = $data['password'];
        $data['password'] = bcrypt($data['password']);

        $user = User::create($data);
        $role_employee = Role::where('name', 'partner')->first();
        $user->roles()->attach($role_employee);

        $data['password'] =  $_password;
        Notification::send($user, new CreateUser($user, $data));

        return response()->json([
            'success' => true,
            'message' => 'Create partner successful',
            'data'=>$user], 200);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updateData(Request $request, User $user){

        $request->validate([
            'commission'=>['required','integer', 'min:0'],
            'accounts'  =>['required','integer', 'min:0'],
            'file'      =>['nullable','mimes:csv,txt'],
        ]);

        $factory = new DataFactoryUpload();
        $factory->upload($request, $user);

        Notification::send($user, new UpdateData($user));

        $user = User::with('last_note')->withCount('accounts')->find($user->id);

        return response()->json(['success' => true,'message'=>'You have successfully update data.','data'=>$user] ,200);
    }
}
