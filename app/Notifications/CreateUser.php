<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Lang;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CreateUser extends Notification implements ShouldQueue
{
    use Queueable;

    protected $user;
    protected $data;

    /**
     * Create a new notification instance.
     *
     * @param $user
     * @param $data
     */
    public function __construct(User $user, $data)
    {
        $this->user = $user;
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mailMessage =   (new MailMessage)
            ->subject(Lang::getFromJson('Account was created'))
            ->line(Lang::getFromJson('You are receiving this email because '.config('app.name').' created an account for you.'));

        if(!empty($this->data['name']))
            $mailMessage->line(Lang::getFromJson('You name: '.$this->data['name']));
        if(!empty($this->data['email']))
            $mailMessage->line(Lang::getFromJson('You email: '.$this->data['email']));
        if(!empty($this->data['password']))
            $mailMessage->line(Lang::getFromJson('You password: '.$this->data['password']));


        return $mailMessage->action(Lang::getFromJson('Log In'), url(config('app.url').route('partner::current.index', $this->user->id, false)));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
