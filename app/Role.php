<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property string description
 * @property string name
 */
class Role extends Model
{
    use SoftDeletes;

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
