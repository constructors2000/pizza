<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 1/8/19
 * Time: 12:51 PM
 */

namespace App\Factorys;

use App\User;
use ArrayIterator;
use League\Csv\Writer;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class DataFactoryDownload
{
    const DIRECTORY = 'files';

    private $full_path_to_dir = '';

    private $user = null;

    /**
     * DataFactoryDownload constructor.
     * @param User|null $user
     */
    public function __construct(User $user = null)
    {
        if(! Storage::disk('public')->exists(self::DIRECTORY) ) {
            Storage::disk('public')->makeDirectory(self::DIRECTORY);
        }
        $this->full_path_to_dir = Storage::disk('public')->path(self::DIRECTORY);

        $this->user = $user;
    }

    /**
     * @param string $manager
     * @param User|null $user
     * @return null|string
     */
    public function download(string $manager, User $user=null){
        if(!empty($user)){
            $this->user = $user;
        }

        if($manager == 'csv'){
            return $this->csv();
        }
        return null;
    }

    protected function csv(){

        $data = $this->prepareData();
        if (empty($data['header']) or empty($data['records'])){
            return null;
        }

        $records = $data['records'];
        $path = $this->full_path_to_dir.'data.csv';

        $writer = Writer::createFromPath($path, 'w+');
        $writer->insertOne($data['header']);
        $writer->insertAll($records); //using an array

        return $path;
    }

    /**
     * @throws \Exception
     */
    private function prepareData(){
        $this->cheackUser();
        $accounts = $this->user->accounts;

        if ($accounts){
            $accounts = $accounts->toArray();
            $header = [];
            foreach ($accounts as $key=>$account){
                $account = array_only($account, [
                    'account_name',
                    'merchant_id',
                    'processing_bank',
                    'close_date',
                    'promo_code',
                    'membership_buy_rate',
                    'transactions',
                    'commission',
                    'status' ]);

                $data = array_divide($account);
                $accounts[$key] =  $data[1];
                if (empty($header)) {
                    $header = $data[0];
                }
            }

            $header = array_map(function($key){
                return str_replace('_', ' ',  ucfirst($key));
            }, $header);

            return ['header'=>$header,'records'=>$accounts];
        } else {
            return [];
        }
    }

    /**
     * @throws \Exception
     */
    private function cheackUser(){
        if(empty($this->user)){
           throw new \Exception('User is empty');
        }

        return true;
    }
}
