<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 1/8/19
 * Time: 12:42 AM
 */

namespace App\Factorys;

use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


use App\User;
use App\Note;
use App\Account;
use League\Csv\Reader;

class DataFactoryUpload
{

    /**
     * The temp repository instance.
     */
    const TEMP_DIRECTORY = 'temp_files';

    private $file_path = '';
    private $user =null;


    public function __construct(){
        if (!Storage::disk('public')->exists(self::TEMP_DIRECTORY)){
            Storage::disk('public')->makeDirectory(self::TEMP_DIRECTORY);
        }
    }

    /**
     *
     * @param $file
     * @throws \Exception
     */
    protected function moveFile($file){
        $newFileName = Uuid::uuid4()->toString() . ($file->guessExtension() ? '.'.$file->guessExtension() : '');
        // get description file name in public driver
        $destinationPath = Storage::disk('public')->path(self::TEMP_DIRECTORY);
        // move file to public storage
        $file->move($destinationPath, $newFileName);

        $this->file_path = $destinationPath.DIRECTORY_SEPARATOR.$newFileName;
    }

    /**
     * Delete all notes. Default used before update
     * @param User|null $_user
     */
    public function deleteOld(User $_user=null){
        $user = $_user ?: $this->user;
        if($user && $user->notes){

            $user->accounts()->delete();

        }
    }

    /**
     *
     * */
    protected function parseData(){
        $csv = Reader::createFromPath($this->file_path, 'r');
        $csv->setHeaderOffset(0);
        $records = $csv->getRecords();

        foreach ($records as $offset => $record) {
            //Get keys
            $keys = array_keys($record);

            //Map keys to format function
            $keys = array_map(function($key){
                return trim(str_replace(' ', '_', str_replace('/', '_', strtolower($key))));
            }, $keys);


            //Use array_combine to map formatted keys to array values
            $data = array_combine($keys,$record);

            $data = array_filter( $data, 'strlen' );

            $data['close_date'] = (!empty($data['close_date'])) ?
                date("Y-m-d H:i:s", strtotime($data['close_date'])): null;
            $data['created_date'] = (!empty($data['created_date'])) ?
                date("Y-m-d H:i:s", strtotime($data['created_date'])): null;
            $data['initial_membership_payment'] = (!empty($data['initial_membership_payment'])) ?
                date("Y-m-d H:i:s", strtotime($data['initial_membership_payment'])): null;

            try {
                $this->saveData($data);
            } catch (\Exception $exception){
                Log::error($exception->getMessage(), $data);
            }
        }
    }

    /**
     *
     * @param array $data
     * @return Account
     */
    protected function saveData(array $data):Account{
        $account = $this->user->accounts()->create($data);
        return $account;
    }

    /**
     * @param Request $request
     * @param User $user
     * @return Note|\Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function upload(Request $request, User $user) {
        $this->user = $user;

        if ($request->has('file')) {
            $this->deleteOld($user);
            $file =$request->file('file');
            $this->moveFile($file);
            $this->parseData();
        }

        $data = $request->only(['accounts', 'commission']);
        $note = $user->notes()->create($data);

        return $note;
    }
}
