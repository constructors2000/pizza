@push('modal')
<!-- Update Partner Settings Modal -->
<div class="modal fade" id="modal-partner-settings" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('Update partner account') }} <span class="user_name"></span> </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('partner::current.update', [null]) }}" id="update_user_partner" data-action="{{ route('partner::current.update', [null]) }}">
                    @csrf
                    <div class="form-group">
                        <label>{{ __('Update Name') }}</label>
                        <input type="text" value="" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Partner Name" required>
                        <span class="invalid-feedback" role="alert">
                            @if ($errors->has('name'))
                                <strong>{{ $errors->first('name') }}</strong>
                            @endif
                        </span>
                    </div>

                    <div class="form-group">
                        <label>{{ __('Update Email Address') }}</label>
                        <input type="text" value="" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email Address" required>
                        <span class="invalid-feedback" role="alert">
                            @if ($errors->has('email'))
                                <strong>{{ $errors->first('email') }}</strong>
                            @endif
                        </span>
                    </div>

                    <div class="form-group">
                        <label>{{ __('Update Password') }} <small>{{ __('leave blank if no changes') }}</small> <small><a href="#" class="generate-new-password">{{ __('or generate new pw') }}</a></small></label>
                        <input type="text" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password">
                        <span class="invalid-feedback" role="alert">
                            @if ($errors->has('password'))
                                    <strong>{{ $errors->first('password') }}</strong>
                            @endif
                        </span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Cancel') }}</button>
                <button type="submit" form="update_user_partner" class="btn btn-primary">{{ __('Save Changes') }}</button>
            </div>
        </div>
    </div>
</div>
<!-- // END Partner Settings Modal -->
@endpush

@push('scripts')
    <script src="{{ asset('js/partner_setting.js') }}"></script>
@endpush
