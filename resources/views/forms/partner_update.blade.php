@push('modal')
<!-- Update Partner Modal -->
<div class="modal fade" id="modal-partner-update" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('Update') }} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ route('partner::current.data.update', ['*']) }}" id="update_data_partner" data-action="{{ route('partner::current.data.update', ['*']) }}" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm">
                            <div class="form-group">
                                <label>{{ __('Total Commission Earned') }}</label>
                                <input type="text" name="commission" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }} form-control-lg" placeholder="Commission" required>
                                <span class="invalid-feedback" role="alert">
                                    @if ($errors->has('commission'))
                                        <strong>{{ $errors->first('commission') }}</strong>
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="form-group">
                                <label>{{ __('New Accounts This Month') }}</label>
                                <input type="text" name="accounts" class="form-control {{ $errors->has('accounts') ? ' is-invalid' : '' }} form-control-lg" placeholder="Number" required>
                                <span class="invalid-feedback" role="alert">
                                    @if ($errors->has('accounts'))
                                        <strong>{{ $errors->first('accounts') }}</strong>
                                    @endif
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Upload New CSV File') }} <small>L{{ __('eave blank to keep current data') }}</small></label>
                        <div class="custom-file">
                            <input type="file" name="file" class="custom-file-input {{ $errors->has('file') ? ' is-invalid' : '' }}" id="csv-file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" placeholder="CSV File">
                            <label class="custom-file-label" for="csv-file">{{ __('Choose file') }}</label>
                            <span class="invalid-feedback" role="alert">
                                @if ($errors->has('file'))
                                <strong>{{ $errors->first('file') }}</strong>
                            @endif
                            </span>
                        </div>
                    </div>
                </form>
                <div class='thin-progress-bar'></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Cancel') }}</button>
                <button type="submit" form="update_data_partner" class="btn btn-primary">{{ __('Save Changes') }}</button>
            </div>
        </div>
    </div>
</div>
<!-- // END Update Partner Modal -->
@endpush

@push('scripts')
    <script src="{{ asset('js/partner_update.js') }}"></script>
@endpush
