<form method="post" action="{{ route('admin::add.partner') }}" id="form-add-partner" class="mt-5">
    @csrf
    <legend class="mb-4">{{ __('Add a Partner') }}</legend>
    <div class="form-group">
        <label>Partner Name</label>
        <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}" placeholder="Partner Name" required>
        <span class="invalid-feedback" role="alert">
            @if ($errors->has('name'))
                <strong>{{ $errors->first('name') }}</strong>
            @endif
        </span>
    </div>
    <div class="form-group">
        <label>{{ __('Email Address') }} <small>is used for login</small></label>
        <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="Email Address" required>
        <span class="invalid-feedback" role="alert">
            @if ($errors->has('email'))
                <strong>{{ $errors->first('email') }}</strong>
            @endif
         </span>

    </div>
    <div class="form-group">
        <label>{{ __('Password') }} <small>{{ __('edit to use custom pw') }}</small></label>
        <input type="text" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" required>
        <span class="invalid-feedback" role="alert">
            @if ($errors->has('password'))
                <strong>{{ $errors->first('password') }}</strong>
            @endif
        </span>
    </div>

    <button type="submit" class="btn btn-primary mt-3">{{ __('Add Partner') }}</button>
</form>

@push('scripts')
    <script src="{{ asset('js/partner_add.js') }}"></script>
@endpush
