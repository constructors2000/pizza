@extends('layouts.app')
@section('class-body', 'page-login')

@section('content')
    <div class="form-wrap">
        <form id="form-signin" method="POST" action="{{ route('login') }}">
            @csrf
            <p class="logo"><img src="{{ asset('svg/logo-pd-icon.svg') }}" alt="Payment Depot"></p>
            <h4>{{ __('Please sign in')}}</h4>

            <div class="form-group">
                <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email Address" required autofocus>
                <span class="invalid-feedback" role="alert">
                    @if ($errors->has('email'))
                        <strong>{{ $errors->first('email') }}</strong>
                    @endif
                </span>
            </div>

            <div class="form-group">
                <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
                <span class="invalid-feedback" role="alert">
                    @if ($errors->has('password'))
                        <strong>{{ $errors->first('password') }}</strong>
                    @endif
                </span>
            </div>


            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Keep me signed in')}}
                </label>

            </div>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Log In</button>
        </form>

        <p><a href="#">{{ __('Become a Partner') }}</a>
            &nbsp;&nbsp;|&nbsp;&nbsp;
            @if (Route::has('password.request'))
                <a  data-toggle="modal" data-target="#modal-reset-password" href="#">{{ __('Lost Password?') }}</a>
            @endif
        </p>

    </div>

    <!-- Reset Password Modal -->
@include('auth.passwords.email')

@endsection
