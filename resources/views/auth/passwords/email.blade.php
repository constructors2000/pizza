
@push('modal')
    <div class="modal fade" id="modal-reset-password" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('Password Recovery') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('password.email') }}" id="reset_form">
                        @csrf
                        <div class="form-group">
                            <label>{{ __('Enter your email address') }}</label>
                            <input id="email" name="email"  type="email" class="form-control form-control-lg" value="{{ old('email') }}" required autofocus>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Cancel') }}</button>
                    <button type="submit" form="reset_form" class="btn btn-primary">{{ __('Submit') }}</button>
                </div>
            </div>
        </div>
    </div>
@endpush
