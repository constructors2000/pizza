@extends('layouts.app')

@section('role', 'Admin')

@section('header')
    @include('admin.header')
@endsection

@section('content')
    <main>
        <section class="admin-main">
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        @include('forms.partner_add')
                    </div>
                    <div class="col-sm-8">
                        @include('admin.partner_list')
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@include('forms.partner_setting')
@include('forms.partner_update')
