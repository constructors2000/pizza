<div class="box">
    <table class="table table-striped" id="table-partners">
        <thead>
        <tr>
            <th>Partner</th>
            <th>Email Address</th>
            <th># of Accounts</th>
            <th>Last Updated</th>
            <th class="table-actions">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr id="user_{{$user->id}}" data-id="{{$user->id}}">
                <td>
                    <a class="name" href="{{route('partner::current.index', [$user->id])}}"> {{$user->name}} </a>
                </td>
                <td>
                    <span class="email">{{$user->email}}</span>
                </td>
                <td>
                    <span class="accounts">{{ $user->accounts_count }}</span>
                </td>
                <td>
                    <span class="updated_at">{{!empty($user->last_note) ? $user->last_note->created_at : '-'}}</span>
                </td>
                <td class="table-actions">
                    <a class="partner-update"   data-toggle="modal" data-target="#modal-partner-update" href="#">Update</a>
                    <a class="partner-settings" data-toggle="modal" data-target="#modal-partner-settings" href="#">Settings</a>
                    <a class="partner-delete"   data-toggle="modal" data-target="#partner-delete" href="{{ $user->link }}"><i class="fas fa-times"></i></a>
                </td>
            </tr>
        @endforeach


        </tbody>
    </table>

</div>

@push('scripts')
    <script src="{{ asset('js/partner_list.js') }}"></script>
@endpush
