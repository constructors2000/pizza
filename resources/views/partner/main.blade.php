@extends('layouts.app')

@section('role', Auth::user()->hasRole('admin') ? 'Admin' : Auth::user()->name)

@section('header')
    @include('partner.header')
@endsection
@section('content')
        <main>
        @include('partner.accounts_list')
        </main>
@endsection

@include('forms.partner_setting')

@push('scripts')
    <script src="{{ asset('js/partner.js') }}"></script>
@endpush
