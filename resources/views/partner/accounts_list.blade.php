<section class="partners-overview">
    <div class="container">
        <div class="box">
            <div class="partner-stats">
                <div class="partner-stat">
                    <strong>{{!empty($user->last_note) ? $user->last_note->commission : '0'}}</strong> Total Commission Earned
                </div>
                <div class="partner-stat">
                    <strong>{{isset($user->accounts_count)? $user->accounts_count : '0'}}</strong> Total # of Accounts
                </div>
                <div class="partner-stat">
                    <strong>{{isset($user->accounts)? round($user->month_commission, 2, PHP_ROUND_HALF_DOWN) : '0'}}</strong> Commission This Month
                </div>
                <div class="partner-stat">
                    <strong>{{!empty($user->last_note) ? $user->last_note->accounts : '0'}}</strong> New Accounts This Month
                </div>
            </div>
        </div>
    </div>
</section>
<section class="partners-accounts">
    <div class="container">
        <div class="section-heading">
            <h3>Accounts History</h3>
            <div class="tools">
                <a href="{{route('partner::current.data.csv', [$user->id])}}" class="btn btn-link">Export CSV</a>
            </div>
        </div>
        <div class="box">
            <div class="table-responsive">
                <table class="table table-striped" id="table-partner-accounts">
                    <thead>
                    <tr>
                        <th>DBA of business <strong>B</strong></th>
                        <th>Merchant ID <strong>C</strong></th>
                        <th>Processor <strong>D</strong></th>
                        <th>Signup <strong>G</strong></th>
                        <th>Promo <strong>M</strong></th>
                        <th>Memership - Buy Rate <strong>Q</strong></th>
                        <th>Trans - Buy Rate <strong>R</strong></th>
                        <th>Trans This Month <strong>S</strong></th>
                        <th>Commission <strong>T</strong></th>
                        <th>Status <strong>U</strong></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user->accounts as $accounts)
                        <tr>
                            <td>{{$accounts->account_name}}</td>
                            <td>{{$accounts->merchant_id}}</td>
                            <td>{{$accounts->processing_bank}}</td>
                            <td>{{$accounts->close_date}}</td>
                            <td>{{$accounts->promo_code}}</td>
                            <td>{{$accounts->membership_buy_rate}}</td>
                            <td>{{$accounts->transaction_buy_rate}}</td>
                            <td>{{$accounts->transactions}}</td>
                            <td>{{$accounts->commission}}</td>
                            <td>{{$accounts->status}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
