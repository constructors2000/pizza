@extends('layouts.app')

@section('content')
    <main>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card text-center">
                        <div class="card-header">
                            <h5 class="">Notification</h5>
                        </div>
                        <div class="card-body ">
                            <p class="card-text">{{ __(empty($message)? '' : $message )}}</p>
                            <a href="{{empty($route)? url(config('app.url')) : route($route) }}" class="btn btn-primary float-right">Next</a>
                        </div>

                        <div class="card-footer text-muted" id="time">
                            {{empty($time) ? '-': $time}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection



@push('scripts')

    @if(!empty($time))
    <script>
        $(document).ready(function () {
            reset = function () {
                window.location.replace('{{empty($route)? url(config('app.url')) : route($route) }}');
            };

            var sec = '{{$time}}';
            var i = sec;

            setTimeout(function run() {
                if (i <= 1) {
                    reset();
                }
                i = i - 1;
                $('#time').html(i);
                setTimeout(run, sec * 100*2);
            }, sec * 100);
        });
    </script>
    @endif
@endpush
