<div class="logo"><a href="/"><img src="{{ asset('svg/logo-pd-icon.svg') }}" alt="Payment Depot"></a>
    <span>Welcome, @yield('role')</span>
</div>
