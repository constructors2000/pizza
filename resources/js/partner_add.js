$(document).ready(start);

function start() {
    var flag_submit = 0;

    NewPasswordInput($("#form-add-partner").find('input[name="password"]'));
    /**
    * SEND FIELDS FOR CREATE NEW PARTNER
    * */
    $("#form-add-partner").on('submit', function (e) {
        e.preventDefault();
        e.stopPropagation();

        if(flag_submit){
            return null;
        }

        flag_submit = 1;

        var form = $(this);
        var link = form.attr('action');

        $.ajax({
            method: "POST",
            url: link,
            data: form.serialize(),
        }).done(function(response) {

            if(response && response.data && response.data.id){
                $('#table-partners').find('tbody').append(
                    '<tr id="user_'+response.data.id+'" data-id="'+response.data.id+'">' +
                    '<td><a class="name" href="'+response.data.link+'">' +response.data.name+ '</a> </td>' +
                    '<td><span class="email">'+response.data.email+' </span> </td>' +
                    '<td><span class="accounts">0</span></td>\n' +
                    '<td><span class="updated_at">-</span></td>\n' +
                    '<td class="table-actions">\n' +
                    '<a class="partner-update"   data-toggle="modal" data-target="#modal-partner-update" href="#">Update</a>\n' +
                    '<a class="partner-settings" data-toggle="modal" data-target="#modal-partner-settings" href="#">Settings</a>\n' +
                    '<a class="partner-delete"   data-toggle="modal" data-target="#partner-delete" href="'+response.data.link+'"><i class="fas fa-times"></i></a>\n' +
                    '</td>'+
                    '<tr>');
                $(form).trigger("reset");
            }
            toastr.success('That partner has been update.','Success');
        }).fail(function (response) {
            if(response && response.responseJSON && response.responseJSON.errors) {
                const errors = response.responseJSON.errors;
                for (var item in errors) {
                    var input = $(form).find("input[name='" + item + "']");
                    $(input).addClass('is-invalid', 2000);
                    $(input).siblings(".invalid-feedback").append('<strong>' + errors[item][0] + '</strong>');
                }
                setTimeout(function () {
                    $('.is-invalid').siblings(".invalid-feedback").empty();
                    $('.is-invalid').removeClass('is-invalid', 2000);
                }, 4000);
            }
            toastr.error('That partner has been fail update.','Error');
        }).always(function() {
            flag_submit = 0;
        });
    })

    /**
    * RESET ADD PARTNER FROM AND GENERATE NEW PASSWORD FOR NEX PARTNER
    * */
    $("#form-add-partner").on('reset', function (e) {
        var form = this;
        setTimeout(function() { NewPasswordInput($(form).find('input[name="password"]'));});
    });

    $('.generate-new-password').on('click', function (e) {
        var input = $(this).closest('form').find('input[name="password"]');
        NewPasswordInput(input);
        e.preventDefault();
        e.stopPropagation();
    });

    /**
     *
     * */
    function NewPasswordInput(input = null) {
        var password = new RandExp(/\w{6,8}[0-9]{1,2}/).gen();
        if (input){
            $(input).val(password);
        }
        return password;
    }

};
