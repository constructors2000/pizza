$(document).ready(start);

function start() {
    NewPasswordInput($("#form-add-partner").find('input[name="password"]'));

    /**
     * GENERATE NEW PASSWORD USE REGEX
     * */
    function NewPasswordInput(input = null) {
        var password = new RandExp(/\w{6,12}/).gen();
        if (input){
            $(input).val(password);
        }
        return password;
    }

    /**
     * SHOW MODAL PARTNER UPDATE DATA AND FILL INPUTS OF MODAL
     * */
    $('.partner-settings').on('click', function (e) {

        var li = $(this).closest('li');
        var div = $(li).find('div');

        var setting = {
            id:$(div).data( "id" ),
            name:$(div).find('.name').html(),
            email: $(div).find('.email').html()
        };

        // This event for update data on update modal (partner_update)
        $(document).trigger('settingHandler', setting);
    });
};
