$(document).ready(start);

function start() {
    /**
     * EVENT: soft delete partner
     * */
    $('#table-partners').on('click', '.partner-delete', function () {
        if(!confirm("Are you sure you want to delete this partner?")){
            return;
        }

        var link = $(this).attr("href");
        var tr = $(this).closest('tr');

        $(tr).attr('disable', true);

        $.ajax({
            method: "DELETE",
            url: link
        }).done(function(response) {
            $(tr).remove();
            toastr.success('That partner has been removed.','Success');
        }).fail(function (response) {
            toastr.success('That partner has been fail remove.','Error');
        }).always(function () {
            $(tr).attr('disable', false);
        });
    });

    /**
    * SHOW MODAL PARTNER SETTING AND FILL INPUTS OF MODAL
    * */
    $('#table-partners').on('click', '.partner-settings', function (e) {

        var tr = $(this).closest('tr')[0];
        var setting = {
            id:$(tr).data( "id" ),
            name:$(tr).find('.name').html(),
            email: $(tr).find('.email').html()
        };

        // This event for update data on setting modal (partner_setting)
        $(document).trigger('settingHandler', setting);
    });

    /**
     * SHOW MODAL PARTNER UPDATE DATA AND FILL INPUTS OF MODAL
     * */
    $('#table-partners').on('click', '.partner-update', function (e) {

        var tr = $(this).closest('tr');
        var setting = {
            id:$(tr).data( "id" ),
            name:$(tr).find('.name').html(),
            email: $(tr).find('.email').html()
        };

        // This event for update data on update modal (partner_update)
        $(document).trigger('updateHandler', setting);
    });

}


