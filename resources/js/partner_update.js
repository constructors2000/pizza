$(document).ready(start);

function start() {
    var flag_submit = 0;
    bsCustomFileInput.init();

    /**
     * SETTING EVENTS
     * */
    $(document).bind("updateHandler", function (event, setting) {
        $('#modal-partner-update').trigger('update', setting);
    });

    /**
     * FILL ALL INPUT OF FORM
     * */
    $('#modal-partner-update').bind('update', function (event, setting) {

        if (setting && setting.id) {
            const form  = $(this).find("form");
            const action = $(form).data("action").replace('/*/', '/'+setting.id+'/');
            $(form).attr('action', action);
        }
    });

    /**
    * HIDE MODAL
    * */
    $('#modal-partner-update').on('hide.bs.modal', function () {
        $(this).find(".invalid-feedback").empty();
        $(this).find('.is-invalid').removeClass('is-invalid');
    });

    /**
    * SUBMIT
    * */
    $('#modal-partner-update form').on('submit', function (e) {
        e.preventDefault();

        if(flag_submit){
            return null;
        }

        flag_submit = 1;

        var form = $(this);
        var link = form.attr('action');
        var formData = new FormData(this);
        $.ajax({
            method: "POST",
            url: link,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            xhr        : function ()
            {
                var jqXHR = null;
                if ( window.ActiveXObject )
                {
                    jqXHR = new window.ActiveXObject( "Microsoft.XMLHTTP" );
                }
                else
                {
                    jqXHR = new window.XMLHttpRequest();
                }
                //Upload progress
                jqXHR.upload.addEventListener( "progress", function ( evt )
                {
                    if ( evt.lengthComputable )
                    {
                        var percentComplete = Math.round( (evt.loaded * 100) / evt.total );
                        //Do something with upload progress
                        setProgressBarWidth(percentComplete);
                        console.log( 'Uploaded percent', percentComplete );
                    }
                }, false );

                return jqXHR;
            },
            beforeSend: function(jqXHR){
                doProgress();
            },
            success: function (xhr) {
                if(xhr && xhr.data && xhr.data.id){
                    $('#user_'+xhr.data.id).find('.accounts').html(xhr.data.accounts_count);
                    if(xhr.data.last_note){
                        $('#user_'+xhr.data.id).find('.updated_at').html(xhr.data.last_note.created_at);
                    }
                }
                $(form).trigger("reset");
                $('#modal-partner-update').modal('hide');
                toastr.success('Data update.','Success');
            },
            error: function(xhr){
                console.error('fail',xhr);
                if(xhr && xhr.responseJSON && xhr.responseJSON.errors) {
                    const errors = xhr.responseJSON.errors;
                    for (var item in errors) {
                        var input = $(form).find("input[name='" + item + "']");
                        $(input).addClass('is-invalid', 2000);
                        $(input).siblings(".invalid-feedback").append('<strong>' + errors[item][0] + '</strong>');
                    }
                    setTimeout(function () {
                        $('.is-invalid').siblings(".invalid-feedback").empty();
                        $('.is-invalid').removeClass('is-invalid', 2000);
                    }, 4000);
                }
                toastr.error('That partner has been fail update.','Error');
            },
            complete: function(xhr) {
                flag_submit = 0;
                doProgress();
            }

        });

    });

    /**
    * PROGRESS BAR
    * */
    setProgressBarWidth = function(width) {
        return $('#modal-partner-update .thin-progress-bar').width(width+'%');
    };

    /**
    * RESET PROGRESS BAR
    * */
    doProgress = function() {
        return setProgressBarWidth("0");
    };
}
