const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .copy('resources/js/admin.js', 'public/js')
    .copy('resources/js/partner_list.js', 'public/js')
    .copy('resources/js/partner_setting.js', 'public/js')
    .copy('resources/js/partner_update.js', 'public/js')
    .copy('resources/js/partner_add.js', 'public/js')
    .copy('resources/js/partner.js', 'public/js')
    .copy('resources/js/partner_header.js', 'public/js')
    .autoload({
        jquery: ['$', 'window.jQuery', 'jQuery'],
    })
    .sass('resources/sass/app.scss', 'public/css');
